Ejercicios sobre JSON
===================
Este ejercico se basa en 4 aplicacion
a la que se puede acceder a través de un activity principal con su respectivos botones

![Screenshot_1483989849.png](https://bitbucket.org/repo/Kjeg8E/images/622407594-Screenshot_1483989849.png)  

.1.Lista de ciudades
---------------
Una lista de ciudades del mundo,

![Screenshot_1483989853.png](https://bitbucket.org/repo/Kjeg8E/images/1968063929-Screenshot_1483989853.png)

Al seleccionar cualquiera te dice el clima actual según http://www.openweathermap.org/

![Screenshot_1483990482.png](https://bitbucket.org/repo/Kjeg8E/images/3282198395-Screenshot_1483990482.png)

.2. Descargar tiempo
-------
Es un buscador, donde pones el nombre de una ciudad y te hace una consulta a http://www.openweathermap.org/ con una respuesta del tiempo que va a hacer de los próximos 7 días.

![Screenshot_1483989864.png](https://bitbucket.org/repo/Kjeg8E/images/3179913777-Screenshot_1483989864.png)

![Screenshot_1483989873.png](https://bitbucket.org/repo/Kjeg8E/images/1190028436-Screenshot_1483989873.png)

y a su vez te guarda en un fichero json y otro xml el resultado de la busqueda.

.3. Conversor de monedas

Es un conversor de monedas que coge los datos del valor actual de cada moneda de https://openexchangerates.org/

![Screenshot_1483989896.png](https://bitbucket.org/repo/Kjeg8E/images/577658537-Screenshot_1483989896.png)

Le he añadido un extra que en vez de poder elegir solo entre euro y dolar, puede elegir entre todas las monedas del mundo soportadas por la api

![Screenshot_1483989902.png](https://bitbucket.org/repo/Kjeg8E/images/638289449-Screenshot_1483989902.png)

![Screenshot_1483989918.png](https://bitbucket.org/repo/Kjeg8E/images/1720480403-Screenshot_1483989918.png)

.4. Wifi browser 
------

Es principalmente una lista de puntos wifi gratuitos de la zona de Málaga brindada en json por http://datosabiertos.malaga.eu/dataset/sedes-wifi-municipales/resource/41c5302a-93f4-4406-949d-349cd502253c

![Screenshot_1483989924.png](https://bitbucket.org/repo/Kjeg8E/images/2901200345-Screenshot_1483989924.png)

Al pulsar en uno de los elementos te lleva a otra activity con información del sitio y un botón para abrir un mapa con las coordenadas que vienen en el json acerca de ese sitio
*(IMPORTANTE: El mapa hay que abrirlo en el explorador web, no con google maps)*

![Screenshot_1483989931.png](https://bitbucket.org/repo/Kjeg8E/images/1182323704-Screenshot_1483989931.png)

![Screenshot_1483989935.png](https://bitbucket.org/repo/Kjeg8E/images/1695965324-Screenshot_1483989935.png)