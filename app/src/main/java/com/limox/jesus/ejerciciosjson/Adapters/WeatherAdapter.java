package com.limox.jesus.ejerciciosjson.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.limox.jesus.ejerciciosjson.Model.Weather;
import com.limox.jesus.ejerciciosjson.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Eva on 08/01/2017.
 */

public class WeatherAdapter extends ArrayAdapter<Weather> {

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");

    public WeatherAdapter(Context context, ArrayList<Weather> list) {
        super(context, R.layout.item_weather,list);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        WeatherHolder holder;
        Weather navItem = getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_weather, parent, false);
            holder = new WeatherHolder();

            holder.mIcon = (ImageView) view.findViewById(R.id.iw_ivwIco);
            holder.mDate = (TextView) view.findViewById(R.id.iw_txvDate);
            holder.mMaxTemp = (TextView) view.findViewById(R.id.iw_txvMaxTemp);
            holder.mMinTemp = (TextView) view.findViewById(R.id.iw_txvMinTemp);
            holder.mMain = (TextView) view.findViewById(R.id.iw_txvMain);
            holder.mDescription = (TextView) view.findViewById(R.id.iw_txvDescription);
            holder.mOthers = (TextView) view.findViewById(R.id.iw_txvOthers);

            view.setTag(holder);
        } else
            holder = (WeatherHolder) view.getTag();

        Picasso.with(getContext()).load(navItem.getIcon()).into(holder.mIcon);
        holder.mDate.setText(dateFormat.format(new Date(navItem.getDate()*1000)));
        holder.mMaxTemp.setText(String.format("%.1f ºC",navItem.getMaxTemp()));
        holder.mMinTemp.setText(String.format("%.1f ºC",navItem.getMinTemp()));
        holder.mMain.setText(navItem.getMain());
        holder.mDescription.setText(navItem.getDescription());
        holder.mOthers.setText("Clouds: "+navItem.getClouds()+"%, "+navItem.getPressure()+" hpa");
        return view;
    }

    private class WeatherHolder {
        private TextView mDate;
        private ImageView mIcon;
        private TextView mMaxTemp;
        private TextView mMinTemp;
        private TextView mMain;
        private TextView mDescription;
        private TextView mOthers;

    }
}
