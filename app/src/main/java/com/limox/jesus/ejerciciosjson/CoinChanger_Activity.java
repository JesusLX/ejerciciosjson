package com.limox.jesus.ejerciciosjson;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.limox.jesus.ejerciciosjson.Model.Coin;
import com.limox.jesus.ejerciciosjson.Utils.Analisis;
import com.limox.jesus.ejerciciosjson.Utils.Calcs;
import com.limox.jesus.ejerciciosjson.Utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CoinChanger_Activity extends AppCompatActivity {

    private static final String WEB = "https://openexchangerates.org/api/latest.json?app_id=59647c1faeb2459392222bcefaf14a3f";
    public static final String TAG = "MyTag";
    ArrayList<Coin> coins;
    RequestQueue mRequestQueue;
    Spinner mSpnValue1;
    Spinner mSpnValue2;
    EditText mEdtValue1;
    EditText mEdtValue2;
    Button mBtnChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin_changer);
        mSpnValue1 = (Spinner) findViewById(R.id.cc_spnValue1);
        mSpnValue2 = (Spinner) findViewById(R.id.cc_spnValue2);
        mBtnChange = (Button) findViewById(R.id.cc_btnConvert);
        mEdtValue1 = (EditText) findViewById(R.id.cc_edtValue1);
        mEdtValue2 = (EditText) findViewById(R.id.cc_edtValue2);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
        mBtnChange.setEnabled(false);
        descarga();

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cc_btnConvert:
                calculate();
                break;
            case R.id.cc_btnReverse:
                reverseSpinners();
                calculate();
                break;

        }
    }

    private void descarga() {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, WEB, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            coins = Analisis.analizarCoins(response);
                            mSpnValue1.setAdapter(new ArrayAdapter<>(CoinChanger_Activity.this, android.R.layout.simple_spinner_dropdown_item, getTitles()));
                            mSpnValue2.setAdapter(new ArrayAdapter<>(CoinChanger_Activity.this, android.R.layout.simple_spinner_dropdown_item, getTitles()));
                            mSpnValue1.setSelection(getSpnPosition("EUR"));
                            mSpnValue2.setSelection(getSpnPosition("USD"));
                            mBtnChange.setEnabled(true);

                        } catch (JSONException e) {
                            e.getMessage();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CoinChanger_Activity.this, error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });


        // Set the tag on the request.
        jsObjRequest.setTag(TAG);
        // Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }

    private CharSequence[] getTitles() {
        CharSequence[] titles = new CharSequence[coins.size()];
        for (int i = 0; i < coins.size(); i++) {
            titles[i] = coins.get(i).getName();
        }
        return titles;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }

    void reverseSpinners() {
        int spn1Pos = mSpnValue1.getSelectedItemPosition();
        mSpnValue1.setSelection(mSpnValue2.getSelectedItemPosition());
        mSpnValue2.setSelection(spn1Pos);
    }

    int getSpnPosition(String key) {
        for (int i = 0; i < coins.size(); i++) {
            if (coins.get(i).getName().equals(key))
                return i;
        }
        return 1;
    }

    void calculate() {
        try {
            mEdtValue2.setText(String.valueOf(Calcs.calculateChange(
                    coins.get(mSpnValue1.getSelectedItemPosition()).getValue(),
                    coins.get(mSpnValue2.getSelectedItemPosition()).getValue(),
                    Double.parseDouble(mEdtValue1.getText().toString()))));
        } catch (Exception e) {
            Toast.makeText(this, "Error al hacer el calculo", Toast.LENGTH_LONG).show();
        }
    }
}
