package com.limox.jesus.ejerciciosjson;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class Home_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void onClick(View view){
        Intent intent = null;
        switch (view.getId()){
            case R.id.btnWeather:
                intent = new Intent(Home_Activity.this,WeatherList_Activity.class);
                break;
            case R.id.btnSaveWeather:
                intent = new Intent(Home_Activity.this,SaveWeather_Activity.class);
                break;
            case R.id.btnCoinChanger:
                intent = new Intent(Home_Activity.this,CoinChanger_Activity.class);
                break;
            case R.id.button4:
                intent = new Intent(Home_Activity.this,WifiPointsList_Activity.class);

                break;
        }
        startActivity(intent);

    }
}
