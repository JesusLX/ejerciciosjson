package com.limox.jesus.ejerciciosjson.Model;

/**
 * Created by jesus on 6/01/17.
 */

public class Coin {
    private String mName;
    private double mValue;

    public Coin() {
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public double getValue() {
        return mValue;
    }

    public void setValue(double value) {
        this.mValue = value;
    }

}
