package com.limox.jesus.ejerciciosjson.Model;

/**
 * Created by jesus on 7/01/17.
 */

public class Weather {
    private long date;
    private String main;
    private String description;
    private double clouds;
    private double mTemp;
    private double mMinTemp;
    private double mMaxTemp;
    private double mHumidity;
    private double mPressure;
    private String icon;


    public double getTemp() {
        return mTemp;
    }

    public void setTemp(double temp) {
        this.mTemp =fahrenheitToCelsius(temp);
    }

    public double getMinTemp() {
        return mMinTemp;
    }

    public void setMinTemp(double minTemp) {
        this.mMinTemp = fahrenheitToCelsius(minTemp);
    }

    public double getMaxTemp() {
        return mMaxTemp;
    }

    public void setMaxTemp(double maxTemp) {
        this.mMaxTemp = fahrenheitToCelsius(maxTemp);
    }

    public double getHumidity() {
        return mHumidity;
    }

    public void setHumidity(double mHumidity) {
        this.mHumidity = mHumidity;
    }

    public double getPressure() {
        return mPressure;
    }

    public void setPressure(double mPressure) {
        this.mPressure = mPressure/10;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = "http://openweathermap.org/img/w/"+icon+".png";
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }


    double fahrenheitToCelsius(Double fahrenheit){
       return (((fahrenheit - 32)*5)/9)/10;
        //return fahrenheit;
    }

    public double getClouds() {
        return clouds;
    }

    public void setClouds(double clouds) {
        this.clouds = clouds;
    }
}
