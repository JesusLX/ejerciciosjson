package com.limox.jesus.ejerciciosjson.Model;

import java.io.Serializable;

/**
 * Created by jesus on 9/01/17.
 */

public class WifiPoint implements Serializable {
    private double mCoordinate1;
    private double mCoordinate2;
    private String mName;
    private String mDescription;

    public double getCoordinate1() {
        return mCoordinate1;
    }

    public void setCoordinate1(double coordinate1) {
        this.mCoordinate1 = coordinate1;
    }

    public double getCoordinate2() {
        return mCoordinate2;
    }

    public void setCoordinate2(double coordinate2) {
        this.mCoordinate2 = coordinate2;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }
}
