package com.limox.jesus.ejerciciosjson;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.limox.jesus.ejerciciosjson.Adapters.WeatherAdapter;
import com.limox.jesus.ejerciciosjson.Model.Weather;
import com.limox.jesus.ejerciciosjson.Utils.Analisis;
import com.limox.jesus.ejerciciosjson.Utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class SaveWeather_Activity extends AppCompatActivity {

    ListView mLvWeather;
    TextInputEditText tietCity;
    RequestQueue mRequestQueue;
    private static final String WEB = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
    private static final String COUNT = "&cnt=7";
    private static final String API_KEY ="&APPID=59fae811066edee64843fe939ce3beaf";
    public static final String TAG = "MyTag";
    public static final String FILE = "miTiempo";
    ArrayList<Weather> mWeather;
    WeatherAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_weather);
        mLvWeather = (ListView) findViewById(R.id.sw_lvWeather);
        tietCity = (TextInputEditText) findViewById(R.id.sw_tietCity);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();

    }
    void onClick(View view){
        descarga(WEB+tietCity.getText()+COUNT+API_KEY);
    }
    private void descarga(String web) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, web, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mWeather = Analisis.analizarWetherArray(response);
                            mAdapter = new WeatherAdapter(SaveWeather_Activity.this,mWeather);
                            mLvWeather.setAdapter(mAdapter);
                            Analisis.escribirJSON(mWeather,FILE+".json");
                            Toast.makeText(SaveWeather_Activity.this, "Ficharo json guardado", Toast.LENGTH_LONG).show();
                            Analisis.crearXML(mWeather,FILE+".xml");
                            Toast.makeText(SaveWeather_Activity.this, "Ficharo xml guardado", Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.getMessage();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(SaveWeather_Activity.this, "Fallo al guardar el archivo", Toast.LENGTH_LONG).show();

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SaveWeather_Activity.this, "Fallo en la descargar", Toast.LENGTH_LONG).show();

                    }
                });


        // Set the tag on the request.
        jsObjRequest.setTag(TAG);
        // Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }
}
