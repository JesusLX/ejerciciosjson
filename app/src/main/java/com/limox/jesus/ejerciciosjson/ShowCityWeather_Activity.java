package com.limox.jesus.ejerciciosjson;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.limox.jesus.ejerciciosjson.Model.Weather;
import com.limox.jesus.ejerciciosjson.Utils.Analisis;
import com.limox.jesus.ejerciciosjson.Utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class ShowCityWeather_Activity extends AppCompatActivity {
    private static final String WEB = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String API_KEY ="&APPID=59fae811066edee64843fe939ce3beaf";
    public static final String TAG = "MyTag";
    String city;
    RequestQueue mRequestQueue;
    Weather mWeather;
    TextView mTxvTemperature,mTxvMinMaxTemperature,mTxvHumidity,mTxvPressure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_city_weather);

        mTxvTemperature = (TextView) findViewById(R.id.swc_txvTemperature);
        mTxvMinMaxTemperature = (TextView) findViewById(R.id.swc_txvMinMaxTemperature);
        mTxvHumidity = (TextView) findViewById(R.id.swc_txvHumidity);
        mTxvPressure = (TextView) findViewById(R.id.swc_txvPressure);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();


        city = getIntent().getStringExtra("city");
        descarga(WEB+city+API_KEY);
    }
    private void descarga(String web) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, web, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mWeather = Analisis.analizarWeather(response);
                            fill();
                        } catch (JSONException e) {
                            e.getMessage();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShowCityWeather_Activity.this, error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });


        // Set the tag on the request.
        jsObjRequest.setTag(TAG);
        // Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }
    void fill(){
        mTxvTemperature.setText(String.format("%.2f ºC",mWeather.getTemp()));
        mTxvMinMaxTemperature.setText(String.format("%1.2f ºC/%2.2f ºC",mWeather.getMinTemp(),mWeather.getMaxTemp()));
        mTxvHumidity.setText(String.valueOf(mWeather.getHumidity())+"%");
        mTxvPressure.setText(String.valueOf(mWeather.getPressure())+" kPa");
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }
}
