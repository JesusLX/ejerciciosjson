package com.limox.jesus.ejerciciosjson.Utils;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import com.limox.jesus.ejerciciosjson.Model.Coin;
import com.limox.jesus.ejerciciosjson.Model.Weather;
import com.limox.jesus.ejerciciosjson.Model.WifiPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by jesus on 6/01/17.
 */

public class Analisis {


    public static ArrayList<Weather> analizarWetherArray(JSONObject jsonObjeto) throws JSONException {
        JSONObject item;
        JSONArray jsonContenido;
        JSONObject temp;
        Weather tmpWeather;
        JSONArray weather;
        ArrayList<Weather> weathers = new ArrayList<>();
        jsonContenido = jsonObjeto.getJSONArray("list");
        for (int i = 0; i < jsonContenido.length(); i++) {
            tmpWeather = new Weather();
            tmpWeather.setDate(((JSONObject) jsonContenido.get(i)).getLong("dt"));
            temp = (JSONObject) ((JSONObject) jsonContenido.get(i)).get("temp");
            tmpWeather.setMaxTemp(temp.getDouble("max"));
            tmpWeather.setMinTemp(temp.getDouble("min"));
            tmpWeather.setPressure(((JSONObject) jsonContenido.get(i)).getInt("pressure"));
            weather = ((JSONObject) jsonContenido.get(i)).getJSONArray("weather");
            tmpWeather.setMain(((JSONObject) weather.get(0)).getString("main"));
            tmpWeather.setDescription(((JSONObject) weather.get(0)).getString("description"));
            tmpWeather.setIcon(((JSONObject) weather.get(0)).getString("icon"));
            tmpWeather.setClouds(((JSONObject) jsonContenido.get(0)).getDouble("clouds"));
            weathers.add(tmpWeather);
        }
        return weathers;
    }
    public static ArrayList<WifiPoint> analizarWifiPointsArray(JSONObject jsonObjeto) throws JSONException {
        JSONObject features;
        JSONArray jsonContenido;
        JSONObject geometry;
        JSONObject properties;
        WifiPoint tmpWifiPoint;
        ArrayList<WifiPoint> wifiPoints = new ArrayList<>();
        jsonContenido = jsonObjeto.getJSONArray("features");
        for (int i = 0; i < jsonContenido.length(); i++) {
           try {

               features = ((JSONObject)jsonContenido.get(i));
               tmpWifiPoint = new WifiPoint();
               geometry = features.getJSONObject("geometry");
               tmpWifiPoint.setCoordinate1(geometry.getJSONArray("coordinates").getDouble(1));
               tmpWifiPoint.setCoordinate2(geometry.getJSONArray("coordinates").getDouble(0));
               properties = features.getJSONObject("properties");
               tmpWifiPoint.setName(properties.getString("TOOLTIP"));
               tmpWifiPoint.setDescription(properties.getString("FINALIDAD"));
               wifiPoints.add(tmpWifiPoint);
           } catch (Exception e){
               e.getMessage();
           }
        }
        return wifiPoints;
    }

    public static ArrayList<Coin> analizarCoins(JSONObject respuesta) throws JSONException {
        JSONObject jCoins;
        JSONObject jCoin;
        Coin coin;
        ArrayList<Coin> coins = new ArrayList<>();
        // añadir contactos (en JSON) a personas
        jCoins = (JSONObject) respuesta.get("rates");
        Iterator<String> iter = jCoins.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {

                coin = new Coin();
                coin.setName(key);
                try {
                    coin.setValue(Double.parseDouble(jCoins.get(key).toString()));
                    coins.add(coin);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            } catch (Exception e) {
                e.getMessage();
            }


        }

        return coins;
    }

    public static Weather analizarWeather(JSONObject response) throws JSONException {
        JSONObject jWeather;

        Weather weather = new Weather();

        jWeather = (JSONObject) response.get("main");
        weather.setTemp(jWeather.getDouble("temp"));
        weather.setPressure(jWeather.getDouble("pressure"));
        weather.setHumidity(jWeather.getDouble("humidity"));
        weather.setMinTemp(jWeather.getDouble("temp_min"));
        weather.setMaxTemp(jWeather.getDouble("temp_max"));


        return weather;

    }

    public static void escribirJSON(ArrayList<Weather> weathers, String fichero) throws IOException, JSONException
    {
        OutputStreamWriter out;
        File miFichero;
        JSONObject objeto, rss, item;
        JSONArray lista;
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        out = new FileWriter(miFichero);
        //crear objeto JSON
        objeto = new JSONObject();

        lista = new JSONArray();
        for (int i = 0; i < weathers.size(); i++) {
            item = new JSONObject();
            item.put("dt",weathers.get(i).getDate());
            item.put("icon",weathers.get(i).getIcon());
            item.put("temp_max",weathers.get(i).getMaxTemp());
            item.put("temp_min",weathers.get(i).getMinTemp());
            item.put("main",weathers.get(i).getMain());
            item.put("description",weathers.get(i).getDescription());
            item.put("clouds",weathers.get(i).getClouds());
            item.put("pressure",weathers.get(i).getPressure());
            lista.put(item);
        }
        objeto.put("weather",lista);
        rss = new JSONObject().put("rss",objeto);
        out.write(rss.toString(4)); //tabulación de 4 caracteres
        out.flush();
        out.close();
        Log.i("info", objeto.toString());
    }
    public static void crearXML(ArrayList<Weather> weathers, String fichero) throws IOException {
        FileOutputStream fout;
        fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero));
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "weathers");
        for (int i = 0; i < weathers.size(); i++) {
            serializer.startTag(null, "weather");
            serializer.attribute(null, "dt", String.valueOf(weathers.get(i).getDate()));
            serializer.startTag(null, "icon");
            serializer.text(weathers.get(i).getIcon().toString());
            serializer.endTag(null, "icon");
            serializer.startTag(null, "temp_max");
            serializer.text(String.valueOf(weathers.get(i).getMaxTemp()));
            serializer.endTag(null, "temp_max");
            serializer.startTag(null, "temp_min");
            serializer.text(String.valueOf(weathers.get(i).getMinTemp()));
            serializer.endTag(null, "temp_min");
            serializer.startTag(null, "main");
            serializer.text(String.valueOf(weathers.get(i).getMain()));
            serializer.endTag(null, "main");
            serializer.startTag(null, "description");
            serializer.text(String.valueOf(weathers.get(i).getDescription()));
            serializer.endTag(null, "description");
            serializer.startTag(null, "clouds");
            serializer.text(String.valueOf(weathers.get(i).getClouds()));
            serializer.endTag(null, "clouds");
            serializer.startTag(null, "pressure");
            serializer.text(String.valueOf(weathers.get(i).getPressure()));
            serializer.endTag(null, "pressure");
            serializer.endTag(null, "weather");
        }
        serializer.endTag(null, "weathers");
        serializer.endDocument();
        serializer.flush();
        fout.close();
    }
}
