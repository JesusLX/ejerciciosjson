package com.limox.jesus.ejerciciosjson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class WeatherList_Activity extends AppCompatActivity {
    String[] mCities;
    ListView mLvCities;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        mLvCities = (ListView) findViewById(R.id.wl_lvCities);
        mCities = new String[]{"Malaga","Madrid","Barcelona","Cadiz","Huelva","Jaen",
                "Cordoba","Zaragoza","Sidney","Paris","Colorado","Texas","Edimburgo","Lisboa","London"};
        mLvCities.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,mCities));
        mLvCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(WeatherList_Activity.this,ShowCityWeather_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("city",mCities[i]);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
