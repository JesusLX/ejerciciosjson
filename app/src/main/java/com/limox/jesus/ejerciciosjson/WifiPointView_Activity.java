package com.limox.jesus.ejerciciosjson;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.limox.jesus.ejerciciosjson.Model.WifiPoint;

public class WifiPointView_Activity extends AppCompatActivity {

    TextView mTxvTitle;
    TextView mTxvDescription;
    Button mBtnOpenMap;
    GoogleMap mMap;
    WifiPoint mWifipoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_point_view);
        mBtnOpenMap = (Button) findViewById(R.id.button);
        mTxvTitle = (TextView) findViewById(R.id.title);
        mTxvDescription = (TextView) findViewById(R.id.description);
        mWifipoint = (WifiPoint) getIntent().getExtras().getSerializable("wifi");

        mTxvTitle.setText( mWifipoint.getName());
        mTxvDescription.setText(mWifipoint.getDescription());
        mBtnOpenMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.es/maps/@"+mWifipoint.getCoordinate1()+","+mWifipoint.getCoordinate2())));
            }
        });

    }
}
