package com.limox.jesus.ejerciciosjson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.limox.jesus.ejerciciosjson.Adapters.WeatherAdapter;
import com.limox.jesus.ejerciciosjson.Model.WifiPoint;
import com.limox.jesus.ejerciciosjson.Utils.Analisis;
import com.limox.jesus.ejerciciosjson.Utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class WifiPointsList_Activity extends AppCompatActivity {
    ListView mlvWifiPoints;
    public static final String TAG = "MyTag";
    public static final String WEB = "http://datosabiertos.malaga.eu/recursos/urbanismoEInfraestructura/equipamientos/sedesWifi.json";
    RequestQueue mRequestQueue;
    ArrayList<WifiPoint> mWifiPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_points_list);
        mlvWifiPoints = (ListView) findViewById(R.id.lvWifiPoints);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
        mlvWifiPoints.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("wifi",mWifiPoints.get(i));
                Intent intent = new Intent(WifiPointsList_Activity.this,WifiPointView_Activity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        descarga(WEB);
    }
    private void descarga(String web) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, web, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mWifiPoints = Analisis.analizarWifiPointsArray(response);
                            mlvWifiPoints.setAdapter(new ArrayAdapter<String >(WifiPointsList_Activity.this,R.layout.support_simple_spinner_dropdown_item,getTitles(mWifiPoints)));

                        } catch (JSONException e) {
                            e.getMessage();
                            Toast.makeText(WifiPointsList_Activity.this, "Fallo al leer el archivo", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(WifiPointsList_Activity.this, "Fallo en la descargar", Toast.LENGTH_LONG).show();

                    }
                });


        // Set the tag on the request.
        jsObjRequest.setTag(TAG);
        // Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }

    String[] getTitles(ArrayList<WifiPoint> wifis){
        String[] titles = new String[wifis.size()];
        for (int i = 0; i < wifis.size(); i++) {
            titles[i] = wifis.get(i).getName();
        }
        return titles;
    }

}
